var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
// var mongo     = require('mongoskin');
var timeout      = require('connect-timeout'); //express v4
// var mongodb   = require('mongodb');




var home         = require('./routes/index');
var gInfo        = require('./routes/group-info');
var fb           = require('./routes/facebook');
var ad           = require('./routes/advertising');
var sentMessages = require('./routes/sent');
var register     = require('./routes/register');
var groups       = require('./routes/groups');
var editProfile  = require('./routes/edit-profile');
var logout       = require('./routes/logout');
var gm           = require('./routes/get-members');
var job          = require('./routes/job');
var fbacct       = require('./routes/fbac');




// var db    = mongo.db("mongodb://localhost:27017/publiciter2", {native_parser:true});
var app        = express();
var mysql      = require('mysql');
var connection = require('express-myconnection'); 
// console.log(db)
// var connection = mysql.createConnection({
//     host     : 'localhost',
//     user     : 'root',
//     password : '',
//     database : 'node'
// });
app.use(function(req,res,next){
    // req.db = connection;
    next(); 
});
app.use(connection(mysql,{
        
        host: 'localhost',
//        user: 'publiciter',
  //      password : 'mkrj23ba',
user: 'root',
password: '',   
     port : '', //port mysql
        database:'node'
    },'request')
);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb'}));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({ secret: 'keyboard cat' }))
app.use(express.static(path.join(__dirname, 'public')));
app.use(timeout(120000));

// action routes
app.use('/', home);
app.use('/group', gInfo);
app.use('/group/:id', gInfo);
app.use('/send', fb);
app.use('/send/:message', fb);

// view routes
app.use('/new', ad);
app.use('/sent', sentMessages);
app.use('/register', register);
app.use('/groups', groups);
app.use('/me', editProfile);
app.use('/logout', logout);
app.use('/getm', gm);
app.use('/jobs', job);
app.use('/fb', fbacct);




// used to store app session

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
