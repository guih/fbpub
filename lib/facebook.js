var cheerio   = require('cheerio');
var request   = require('request');
var utils     = require('util');
var appStatic = require('../lib/app-static');


exports.grabTpken = function(){
	return this.token;
}
exports.cookieF = function(){
	return this.cookieJar;
};


exports.cookieL = function(){
	return this.cookieJarL;
};

exports.getCookie = function(fn){

	// reference to 'this' object
	var _that         = this;
	// our target to get login cookie
	var loginUrl      = "https://m.facebook.com/login.php";
	// url where cookies will be applied
	var applyCookieTo = "https://facebook.com/";

	// check if this.coockieJar is already got
	// so use cache instead request another :)
	if( ! this.cookieJar ){
		request({
		    method	: 'POST',
		    uri 	: loginUrl,
		},
		function(err, res, body) {
		    if (err) { return console.log(err) };

		    // header 'set-cookie' property
		    var myCookie = request.cookie(res.headers['set-cookie'][0]);

		    // instace new request jar file
		    var cookieJar = request.jar();
		    // sets our instance with our cookie and set it to our url
		    cookieJar.setCookie(myCookie, applyCookieTo);
		    // cache cookieJar
		    _that.cookieJar = cookieJar;
		    _that.cookieStr = res.headers['set-cookie'][0];

		    // callback...
			fn(cookieJar, this.cookieStr);
		});		
	}else{
		// console.log('CACHED COOKIE, getCoookie');
		fn(this.cookieJar, _that.cookieStr);
	}
};

exports.login = function(fn){

	var _that = this;

	this.getCookie(function(cookie){
			console.log(' 1 LOGIN....')
		var getUserId = function(fn){
			request({
				method	: 'GET',
				uri 	: 'https://www.facebook.com/profile.php?',
				jar 	: _that.cookieJar,
				headers : {
					'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'  
				}
			},
			function(err, res, body) {
				if (err) { return console.log(err) };

				var myEnd = body.match(/("viewer":([^=]*),"rsp")/g)[0]
				.replace(',"rsp"', '')
				.replace('"viewer":', '');
				var endId = myEnd;

				fn(endId);
			});
			
		};
		if( !_that.cookieJarL && !_that.userId ){
		    request({
			    method: 'POST',
			    uri: 'https://m.facebook.com/login.php',
			    form: {
			    	email 	: appStatic.getEmail(),
			    	pass 	: appStatic.getPassword(),
			    	login	: "Entrar"
			    },
		        jar: cookie
		    },
		    function(err, res, body) {
		        if (err) { return console.log(err) };
		        _that.cookieJarL = cookie;
		        

		        getUserId(function(uid){
	        		fn({
	        			cookie : cookie,
	        			userId : uid,
	        			cookieStr: res.headers['set-cookie'][0]
	        		});
	        		_that.userId = uid;
		        });
		    });
		}else{
			// console.log('CACHED COOKIE, login');
			fn({
				cookie : _that.cookieJarL,
				userId : _that.userId,
				cookieStr: _that.cookieStr
	        });
		}
	});
};
exports.getRealId = function(cookie, id, fn){
	var url = utils.format( 'https://www.facebook.com/app_scoped_user_id/%s', id );
	request({
	    method	: 'HEAD',
	    uri 	: url,
        jar 	: cookie,
        headers : {
		    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'
		}
    },
    function(err, res, body) {
        if (err) { return console.log(err) };
// fbid
		request({
		    method	: 'GET',
		    uri 	: 'https://facebook.com' + res.req.path,
	        jar 	: cookie,
	        headers : {
			    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'
			}
	    },
	    function(err, res, body) {
	        if (err) { return console.log(err) };
	// fbid
			// console.log('********************************************************************',body.split('"uid":{')[0])
			// var eof = "";
			// if( body.split(';profile_owner&quot;:&quot;')[1] ){
			// 	eof =  body.split(';profile_owner&quot;:&quot;')[1].split('&quot;,&quot;ref&')[0]
	  //   	}else{
	  //   		eof = 
	  //   	}
	  // console.log( body.split(';profile_owner&quot;:&quot;')[1].split('&quot;,&quot;ref&')[0])
	    	 fn(  body.split(';profile_owner&quot;:&quot;')[1].split('&quot;,&quot;ref&')[0]);
	    });	

    });		

}
exports.sendMessage = function(to, msg, fn){

	this.to   = to;
	this.msg  = msg;	
	var _that = this;
	console.log(to instanceof Array)
	if( to instanceof Array ){
		this.login(function(data){
			for( var i = 0; i< to.length; i++){
				var reciver = to[i];

				// console.log(to[i]);
				// return;
				// _that.getRealId(cookie, reciver, function(toId){
					console.log('  2 getting real id...')
					_that.getFormData(reciver, data.cookie, msg,  function(data){
						console.log(' 3 getting form data...')

						var mData = JSON.parse( data.replace( ',}', '}' ) );

						// console.log(mData)
						if( mData.tids != null ){

							_that.removeMessage(mData.tids, data.cookie, function(status){
								console.log(' - removing message...')
								console.log(to[i])
								if( to[i] != undefined ){
									_that.sendMessage( to[i].id, msg, function(res){
										console.log(' - trying again after delete message');

										fn(res);
									});							
								}

							});
				
						}else{
						mData.body = msg;
							// console.log(' 4 message sent requested!...')
						console.log(' 4 message sent requested!... for', reciver)


							request({
							    method	: 'POST',
							    uri 	: 'https://m.facebook.com/messages/send/?icm=1',
							    form 	: mData,
						        jar 	: data.cookie
						    },
						    function(err, res, body) {
						        if (err) { return console.log(err) };
								console.log(' 5 message sent!...')

					        	fn({ success : "Message sent ok!" });
						    });		
						    return;		
						}
					});
				// });
			}

		});			
	}else{
		// Activate later
		this.login(function(data){
			_that.getRealId(data.cookie, to, function(toId){
				console.log('  2 getting real id...')
				_that.getFormData(toId, data.cookie, msg,  function(data){
					console.log(' 3 getting form data...')

					var mData = JSON.parse( data.replace( ',}', '}' ) );

					// console.log(mData)
					if( mData.tids != null ){

						_that.removeMessage(mData.tids, data.cookie, function(status){
							console.log(' - removing message...')
							// console.log(status)
							_that.sendMessage( to, msg, function(res){
								console.log(' - trying again after delete message');

								fn(res);
							});
						});
			
					}else{
						mData.body = msg;
						// console.log(' 4 message sent requested!... for', toId)

						request({
						    method	: 'POST',
						    uri 	: 'https://m.facebook.com/messages/send/?icm=1',
						    form 	: mData,
					        jar 	: data.cookie
					    },
					    function(err, res, body) {
					        if (err) { return console.log(err) };
							console.log(' 5 message sent!...')

				        	fn({ success : "Message sent ok!" });
					    });				
					}
				});
			});
		});		
	}



};
exports.exchangeToken = function(cookie, args, fn){

	var _that = this;
	var client_id = args.client_id;
	var redirect = args.redirect;
	var secret = args.secret;
	var code = args.code;

	var url = utils.format('https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s',
		client_id,
		redirect,
		secret,
		code
	);
	console.log(' 3 EXCHANGING...')

	request({
	    method	: 'GET',
	    uri 	:  url,
        jar 	: cookie
    },
    function(err, res, body) {

        if (err) { return console.log(err) };
    	// console.log(body)
		fn(body.split('=')[1].split('&')[0]);
    });	
};


exports.getAcessToken = function(fn){
	var _that =this;
	if( !this.token ){
		this.login(function(cookie){

			var headers = {
			    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'
			};


			var options = {
				//url: 'https://www.facebook.com/dialog/oauth?type=user_agent&client_id=999365160092026&redirect_uri=http%3A%2F%2Flocal.host%3A3000%2F&scope=',
				url :'https://graph.facebook.com/oauth/authorize?grant_type=client_credentials&client_id=999365160092026&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2F',
				headers: headers,
				type: "GET",
				jar: cookie
			};

			function callback(error, response, body) {
			    // console.log(response)
			    if (!error && response.statusCode == 200) {
				    console.log(response.request.uri.query)

			        _that.exchangeToken(cookie, {
				        	client_id: '999365160092026',
				        	redirect: 'http%3A%2F%2Flocalhost%3A3000%2F',
				        	secret: '041e1152c469b7e5bf6c92a74653e74a',
				        	code: response.request.uri.query.split('=')[1]
				        }
				       	,function(accessToken){
				        	if( typeof fn == "function" ){
				        		fn.call(null, accessToken);
				        	}
				    });

			    }
			}

			request(options, callback);
		});		
	}else{
		fn(this.token);
	}

}

exports.removeMessage = function(tid, cookie, fn){
	var _that = this,
	url   = utils.format( 
		'https://m.facebook.com/messages/read/?tid=%s&refid=11#fua', 
		tid
	);
	// console.log(url)

	_that.getDeleteToken( url, cookie, function(token){
		// console.log('TOKEN', token)
		_that.deleteMessage(token, tid, cookie, function(done){
			fn(done);
		});
	});
};
exports.getFormData = function(to, cookie, msg, fn){
		var _that = this;

		request({
		    method	: 'GET',
		    uri 	: 'https://m.facebook.com/messages/thread/'+to+'/?refid=17',
	        jar 	: cookie
	    },
	    function(err, res, body) {
	    	// console.log(body);

	        if (err) { return console.log(err) };

	        var $ = cheerio.load(body);
	        $('#composer_form').find('textarea').val( msg )
	        var fields = $('#composer_form').find('textarea, input');

	        var fieldsParsed = {};

	        for( var i = 0; i < fields.length; i++ ){
	        	fieldsParsed[i] = fields[i].attribs;
	        }

	        fn( _that.formatFormSession(fieldsParsed) );
	       
	    });
}
exports.formatFormSession = function(items){
	var eos = "";

	eos += "{";
	for( i in items ){

		eos += utils.format( '"%s":"%s",', 
			items[i].name, 
			encodeURIComponent( items[i].value ) 
		); 
	}
	eos += "}";

	return eos;
};

exports.getDeleteToken = function(uri, cookie, fn){

		var _that = this;

		request({
		    method: 'GET',
		    uri: uri,
	        jar: cookie
	    },
	    function(err, res, body) {

	        if (err) { return console.log(err) };
			
			var $ = cheerio.load(body);
			var token = $('#u_0_0').find('input[name="fb_dtsg"]')[0].attribs.value;

			fn(token);

	    });	
};


exports.deleteMessage = function(token, tid, cookie, fn){

	var _that = this;

	request({
	    method: 'POST',
	    uri: utils.format( 
	    	'https://m.facebook.com/messages/action_redirect?tid=%s&start=0&refid=12', 
	    	tid
	    ),
	    form:{
			'fb_dtsg'		: token,
			'charset_test'	: '%E2%82%AC%2C%C2%B4%2C%E2%82%AC%2C%C2%B4%2C%E6%B0%B4%2C%D0%94%2C%D0%84',
			'delete'		: 'Excluir'
	    },
        jar: cookie
    },
    function(err, res, body) {

        if (err) { return console.log(err) };

        // console.log('location =>', res.headers.location)
// https://m.facebook.com/messages/action/?mm_action=delete&tids=mid.1425145886098%3A5e335b604eb39c3664&gfid=AQCyanlhXuKC4vNF

        if( res.headers.location ){

			request({
			    method	: 'GET',
			    uri 	: res.headers.location,
		        jar 	: cookie
		    },
		    function(err, res, body) {

		       if (err) { return console.log(err) };

				var $ = cheerio.load(body);
				var deleteLink = body.split('class="bh bj"')[0].split('>Excluir<')[0].split('Cancelar')[1].split('href="')[1].split('"')[0];

				var deleteLinkChunked = deleteLink.split("&amp;");
				deleteLink = deleteLinkChunked.join('&');
				request({
				    method	: 'GET',
				    uri 	: utils.format( 'https://m.facebook.com/%s', deleteLink ),
			        jar 	: cookie
			    },
			    function(err, res, body) {

			        if (err) { return console.log(err) };
			        if( body.length > 0 ){
			        	// console.log('REMOVE BODY =>', body)/
			       		fn({success: "Message deleted!"});
			        }
			    });	

		    });	
		}
    });	
};