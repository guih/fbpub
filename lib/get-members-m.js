var cheerio  = require('cheerio');
var utils    = require('util');
var async    = require('async');
var request  = require('request');
var user     = require('./user');
var facebook = require('./facebook');

exports.getMembers = function(db, groupId, fn){
		var _that = this;

		this.end = {};
		this.db = db;

		facebook.login(function(cookie){
			_that.groupId   = groupId;
			_that.cookieJar = cookie;
			// console.log('fetcing payload....')
			_that.fetchPayload(function(ids){
				fn(ids);				
			});
		});

};
exports.getMemberCount = function(groupId, fn){
	var _that = this;

	request({
		method	: 'GET',
		uri 	: utils.format(
			'https://touch.facebook.com/groups/%d?view=info',
			_that.groupId
		),
		jar 	: _that.cookieJar
	},
	function(err, res, body) {
		if (err) { return console.log(err) };

		var memberCount = body
		.split('membros\\u003C\\/h3>\\u003Ca class=\\"_')[0]
		.split('h3 id=\\"u_0_');

		var postToken = body.split('{"dtsg":{"token":"')[1].split('","valid_for":');
		// console.log(postToken)
		var item = memberCount[memberCount.length-1]
		.split('\">');
		var end = { 
			mToken: '1Z3p40x846AeDgy78qzoC6ErwgEmwBwRyE5Wdw', 
			postToken: postToken 
		};	

		if( item[1] != 0 )
			end.memberCount =  item[1].replace('.','')

		fn( end )

	});	

};

exports.fetchPayload = function(fn){

	var i   = 0,
	nM      = 0,
	sum     = 0,
	start   = 0,
	_lock   = true,	
	_that   = this,
	idsEnd  = "",
	theIds  = [],
	theKeys = 0;

	_that.getMemberCount(_that.groupId, function(total){
		// var totalDivided = Math.round((parseInt(total.memberCount)/100));
		console.log()
		return;
		if (total.memberCount ==0 || total.memberCount ==null) {
			throw "Err memberCount invalid";
			return;
		}

		nM = parseInt(total.memberCount);
		console.log('Meber count => ', nM)
		if (nM < 30) {
		// 	sum = 0;
			console.log('make request for a single page')
		} else {

			sum =  (nM < 100) ? Math.round((nM / 10)) : Math.round((nM / 30));
			// console.log(sum)
			// return false;

			async.whilst(function () {
				return i <= sum;
			},
			function (next) {

				start = i*30;

				_that.getPagination( start, function(body){
					// console.log(body);
					// var endStr   = body.replace('for (;;);', '');
					var matchedIds  = body.match(/id=\"member_([^"]*\")/g);
					// console.log('OK ids found', matchedIds)

					// if( matchedIds == null )
					// 	i = (Math.round(parseInt(total.memberCount)/100)-1);
					console.log(matchedIds)

					_that.end[i] = matchedIds;
					// // console.log(_that.end)
					i++;
					next();
				});
			},
			function (err) {
			
			
				theKeys = Object.keys(_that.end);
				theIds  = [];

				for( var j = 0; j<theKeys.length; j++  )
					if( _that.end[j] != null )
						for(var o =0; o< _that.end[j].length; o++)
							theIds.push(_that.end[j][o].replace('id=\"member_', '').replace('\"', ''))


				console.log(theIds);
				console.log('done! every page checked');
				return false;
				// ONTEM EU AQUI CONSEGUI ENVIAR 500 INBOX AI O TYLER FOI BLOQUEADo
			
			// facebook.sendMessage( myFinal, '#{TEXT}', function(re){
			// 	console.log('Sent to => ', re)
			// } )

			});
		}
	});
};

exports.getPagination = function(current, fn){
	console.log('TRYNG WITH', current)
	var _that = this;

	var url = utils.format(
		'https://m.facebook.com/browse/group/members/?id=%s&start=%d',
		// 'https://touch.facebook.com/browse/group/members/?id=%s&start=%d&__ajax__=&__user=%d',
			_that.groupId,
			current
			// ,
			// 100009181512275
	);
	console.log(url)

	request({
		method	: 'GET',
		uri 	: url,
		jar 	: _that.cookieJar
	}, function(err, res, body) {
		if (err) { return console.log(err) };
		
		fn(body)
	})
};