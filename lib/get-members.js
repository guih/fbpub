var cheerio  = require('cheerio');
var utils    = require('util');
var async    = require('async');
var request  = require('request');
var user     = require('./user');
var facebook = require('./facebook');
var fs       = require('fs');
var mixin    = require('util')._extend;

exports.getMembers = function(db, groupId, fn){
		var _that = this;

		this.end = {};
		this.db = db;

		facebook.login(function(data){
			_that.groupId   = groupId;
			_that.cookieJar = data.cookie;
			_that.userId = data.userId
			// console.log('fetcing payload....')
			_that.fetchPayload(function(ids){
				fn(ids);				
			});
		});

};
exports.isPublicGroup = function(fn){
	var _that = this;

	request({
		method	: 'GET',
		uri 	: utils.format(
			'https://www.facebook.com/groups/%s/',
			_that.groupId
		),
		jar 	: _that.cookieJar,
		headers : {
		    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'  
		}
	},
	function(err, res, body) {
		if (err) { return console.log(err) };

		var end = false;

		var chunked = body.match(/(ajaxify="\/ajax\/b)/);

		if( chunked != null )
			end = true;
	
		fn(end);
	});		
};
exports.getMemberCount = function(groupId, fn){
	console.log('get member count')
	var _that = this;

	request({
		method	: 'GET',
		uri 	: utils.format(
			'https://touch.facebook.com/groups/%d?view=info',
			_that.groupId
		),
		jar 	: _that.cookieJar
	},
	function(err, res, body) {
		if (err) { return console.log(err) };

		// console.log(body);
		var chunked = body
		.match(/(\d+([\d,]?\d)*(\.\d+)? Members)/g)[0];

		var title = body
		.match(/<title>(.*?)<\/title>/)[0]
		.replace('<title>', '')
		.replace('</title>', '');
		// var groupInsertion = {
		// 	'scoped_id'	:'',
		// 	'name'		:'',
		// 	'total'		:'',
		// 	'fetched'	:'',
		// 	'added_on'	:'',
		// 	'owner'		:'',
		// 	'ids'		:'',		
		// }


		var memberCount = chunked
		.split(' ')[0].replace(',', '');
		
		// .split('Members\\u003C\\/h3>\\u003Ca class=\\"_5r7n\\" href=\\"\\/groups\\/');

		var postToken = body
		.match(/token([^.]{15})/g)[0]
		.split('":"')[1];

		var groupId = body
		.match(/(\?group_id=\d+([\d,]?\d)*(\.\d+)?)/m)[0]
		.replace('?group_id=', '')
		// // console.log(postToken)
		// var item = memberCount[memberCount.length-1]
		// .split('\">');
		var end = { 
			mToken: '1Z3p40x846AeDgy78qzoC6ErwgEmwBwRyE5Wdw', 
			postToken: postToken 
		};	

		// if( item[1] != 0 )
			end.memberCount = memberCount;
			end.title = title;
			end.groupId = groupId;



		fn( end )
		// console.log(end);

	});	

};

exports.fetchPayload = function(fn){

	var i   = 0,
	nM      = 0,
	sum     = 0,
	start   = 0,
	_lock   = true,	
	_that   = this,
	idsEnd  = "",
	theIds  = [],
	theKeys = 0;
	_that.isPublicGroup(function(isPublic){
		if( isPublic ){
			console.log('is public');
			_that.getMemberCount(_that.groupId, function(total){
				// var totalDivided = Math.round((parseInt(total.memberCount)/100));
				console.log(total)
				// return;
				// user.addGroupUser()


				if (total.memberCount ==0 || total.memberCount ==null) {
					throw "Err memberCount invalid";
					return;
				}

				nM = parseInt(total.memberCount);

				if (nM < 30) {
					var getSinglePriv8 = function(data, gid){

						var headers = {
						    'accept-language': 'en-US,en;q=0.8',
						    'user-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36',
						    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
						    'referer': 'https://www.facebook.com/groups/'+data.gid+'/',
						   	};

						var options = {
						    url: 'https://www.facebook.com/ajax/browser/dialog/group_members/?gid='+data.gid+'&edge=groups%3Amembers&__asyncDialog=3&__user='+data.userId+'&__a=1&__dyn=',
						    headers: headers,
						    jar 	: data.cookie
						};

						function callback(error, response, body) {
						    if (!error && response.statusCode == 200) {
						       	var bodyChunked = body.replace('for (;;);', '');
						       	var end = JSON.parse( bodyChunked ).jsmods.markup[0][1].__html;
						       	var myIds = end.match(/(eid="([^=]*"))/g);
						       	var endParsed = [];

						       	for( var l = 0; l < myIds.length; l++ ){
							       	endParsed.push(myIds[l]
							       	.replace('eid="', '')
							       	.replace('"', ''));
							    }


						       fn(mixin(total, {ids: endParsed}));
						    	//t
						    }
						}

						request(options, callback);	
					}

					facebook.login(function(data){
						data.gid = _that.groupId;
						getSinglePriv8(data, function(results){
							fn(resilts);
						});
					})

				// 	sum = 0;
					// fn(mixin(total,{ids:[6556565656,55658989898,89595895895489,89489489489,56565648949,6556565656,55658989898,89595895895489,89489489489,56565648949]}))
				} else {

					sum =  (nM < 100) ? Math.round((nM / 10)) : Math.round((nM / 100));

					async.whilst(function () {
						return i <= (Math.round(parseInt(total.memberCount)/100)-1);
					},
					function (next) {

						console.log('TOTAL EQUATION =>', (Math.round(parseInt(total.memberCount)/100)-1));
						start = i*99;

						// if( start > 1000 )
						// 	i = (Math.round(parseInt(total.memberCount)/100)-1)+2;

						var args = {
							start: start,
							url: utils.format(
								'https://www.facebook.com/ajax/browser/list/group_members/?id=%s&gid=%s&edge=groups%3Amembers&order=default&view=list&start=%s&__user=%s&__a=1&__dyn=&__req=z&__rev=1647211',
								_that.groupId,
								_that.groupId,
								start,
								_that.userId
							),
							lock: 0
						}



						_that.getPagination( args, function(body){
							var endStr   = body.replace('for (;;);', '');
							var matchedIds  = endStr.match(/profileid=([^=]*)\\"/g);
						console.log('OK ids found', matchedIds)

							if( matchedIds == null )
								i = (Math.round(parseInt(total.memberCount)/100)-1)+2;


							_that.end[i] = matchedIds;
							// // console.log(_that.end)
							i=i+1;
							next();
						});
					},
					function (err) {
					
					
						theKeys = Object.keys(_that.end);
						theIds  = [];

						for( var j = 0; j<theKeys.length; j++  )
							if( _that.end[j] != null )
								for( var o =0; o< _that.end[j].length; o++ )
									theIds.push( _that.end[j][o].replace('profileid=\\"', '').replace('\\"', '') )


						if( _that.end.length > 200 )
							i = (Math.round(parseInt(total.memberCount)/100)-1)+2;
							
						fn(mixin(total,{ids:theIds}))
						

						// console.log(theIds,mixin(total,{ids:theIds}));
						console.log('done!')
						// fn(theIds)
						// return false;
						// ONTEM EU AQUI CONSEGUI ENVIAR 500 INBOX AI O TYLER FOI BLOQUEADo
					
						// facebook.sendMessage( myFinal, '#{TEXT}', function(re){
						// 	console.log('Sent to => ', re)
						// } )

					});
				}
			});
		}else{
			console.log('not public');

			_that.getMemberCount(_that.groupId, function(total){
				// var totalDivided = Math.round((parseInt(total.memberCount)/100));
				// console.log(total.memberCount)
				// return;
				console.log(total)

				if (total.memberCount ==0 || total.memberCount ==null) {
					throw "Err memberCount invalid";
					return;
				}

				nM = parseInt(total.memberCount);

				if (nM < 30) {

					console.log('preiv8 joined')

				} else {

					sum =  (nM < 100) ? Math.round((nM / 10)) : Math.round((nM / 100));

					async.whilst(function () {
						return i <= (Math.round(parseInt(total.memberCount)/100)-1);
					},
					function (next) {

						start = i*30;
						var args = {
							start: start,
							url: utils.format(
								'https://touch.facebook.com/browse/group/members/?id=%s&start=%d&__ajax__=&__user=%d',
								_that.groupId,
								start,
								_that.userId
							),
							lock: 0
						}

						_that.getPagination( args, function(body){
							var endStr   = body.replace('for (;;);', '');
							var matchedIds  = endStr.match(/id=\\"member_([^"]*\\")/g);
							console.log('OK ids found', matchedIds)

							// if( matchedIds == null )
							// 	i = (Math.round(parseInt(total.memberCount)/100)-1);
							if( i*30 == 210 )
								i = (Math.round(parseInt(total.memberCount)/100)-1)+2;

							_that.end[i] = matchedIds;
							// console.log(_that.end)
							i=i+1;
							next();
						});
					},
					function (err) {
					
					
						theKeys = Object.keys(_that.end);
						theIds  = [];

						for( var j = 0; j<theKeys.length; j++  )
							if( _that.end[j] != null )
								for( var o =0; o< _that.end[j].length; o++ )
									theIds.push( _that.end[j][o].replace('id=\\"member_', '').replace('\\"', '') )



						fn(mixin(total,{ids:theIds}))

						console.log(theIds);
						console.log('done!')
						return false;
						// ONTEM EU AQUI CONSEGUI ENVIAR 500 INBOX AI O TYLER FOI BLOQUEADo
					
						// facebook.sendMessage( myFinal, '#{TEXT}', function(re){
						// 	console.log('Sent to => ', re)
						// } )

					});
				}
			});
		}		
	});
};

exports.getPagination = function(args, fn){
	console.log('Start in =>', args.start)
	var _that = this;

	var url = args.url;
	var current = args.start;

	// facebook.login(function(cookie){
		request({
			method	: 'GET',
			uri 	: url,
			jar 	: _that.cookieJar
		}, function(err, res, body) {
			if (err) { return console.log(err) };
			
			fn(body)
		})
	// });
};