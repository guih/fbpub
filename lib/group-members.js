var request   = require('request');
var utils     = require('util');
var appStatic = require('../lib/app-static');
var facebook = require('../lib/facebook');




exports.fetchMembersByGroupId = function(groupId, fn){
	facebook.getAcessToken(function(token){
		var query = utils.format( 
			"SELECT uid FROM group_member WHERE gid ='%d'", groupId );
		// var url = utils.format( 
		// 	'https://graph.facebook.com/v2.0/fql?access_token=%s' +
		// 	'&format=json&method=get&pretty=0&q=%s&suppress_http_code=1',
		// 	token,
		// 	query
		// );
		var url = utils.format(
			'https://graph.facebook.com/v2.0/%s/members?access_token=%s&format=json&limit=5000&method=get&offset=0&pretty=0&suppress_http_code=1',
			groupId,
			token
		);
		// console.log(url)
		// facebook.getAcessToken();
		request.get(url, {}, function (error, response, body) {
	        if (!error && response.statusCode == 200) {
	        	fn( JSON.parse( body ) );
	        }
	        if (response.statusCode == 400) {
	            fn( {eror: "Invalid/Expired token."} );
	        }
	    });			
	});

};