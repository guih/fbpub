var request      = require('request');
var utils        = require('util');
var mixin        = require('util')._extend;
var appStatic    = require('../lib/app-static');
var groupMembers = require('../lib/group-members');
var facebook     = require('../lib/facebook');



exports.seek = function(groupAddress, acessToken, fn){
	var _that = this;
	this.groupAlias = groupAddress || "";
	var gData = {};

	facebook.getAcessToken(function(token){
		
		_that.acessToken = token;

		if( _that.isNumber( groupAddress ) ){
			_that.fetchDetailsById(token, groupAddress, function(groupMeta){
				groupMembers.fetchMembersByGroupId( groupAddress, function(data){
					var end =  mixin( groupMeta, data );
					fn( end );
				});
			});
		}else{
			_that.fetchDetails(token, function(body){
				// console.log('BODY',body)
				if(  body == undefined){
					fn({error: "Grupo não encontrado."})

				}else{
					groupMembers.fetchMembersByGroupId( body.id, function(data){
						fn( mixin( body, data ) );
					});		
				}
			});
		}
	});

};
exports.fetchDetailsById = function(token, id, fn){
	var url = utils.format(
		'https://graph.facebook.com/v2.2/%s?access_token=%s&format=json&method=get&pretty=0&suppress_http_code=1',
		id,
		token
	);
	// console.log(url)
	// facebook.getAcessToken();
	request.get(url, {}, function (error, response, body) {
        if (!error && response.statusCode == 200) {
        	console.log(body)
        	fn( JSON.parse( body ) );
        }
        if (response.statusCode == 400) {
            fn( {eror: "Invalid/Expired token."} );
        }
    });	

}

exports.fetchDetails = function(acessToken, fn){

	var url = utils.format( 
		"https://graph.facebook.com/search?q=%s&type=group&access_token=%s",
		this.groupAlias,
		acessToken
	),
	options = { 
		method: 'GET',
    },
    nSuper = this;


	request(url, options, function (error, response, body) {
		// console.log(response)
	        if (!error && response.statusCode == 200) {
	        	// console.log('',body)
	            fn( nSuper.data = JSON.parse( body ).data[0]  );
	        }
	        if (response.statusCode == 400) {
	            fn( {eror: "Invalid/Expired token."} );
	        }
	    }
	);	
};
exports.getGroupByName = function(gname, fn){
	var _that = this;
	facebook.login(function(cookie){
		var url = utils.format('https://www.facebook.com/groups/%s',
			gname
		);
		console.log(' 3 geting id by name of group......')

		request({
		    method	: 'GET',
		    uri 	:  url,
			jar 	: cookie,
			headers : {
				'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36'  
			}
	    },
	    function(err, res, body) {

	        if (err) { return console.log(err) };
	    	// console.log(body)
	    	// console.log(body)
	    	var idMarkup = body.match(/\"group_id.+\d,"r/g)[0];
	    	var groupNumericId = idMarkup.replace('"group_id":', '').replace(',"r', '');

	    	if( typeof fn == "function" )
	    		fn(groupNumericId);
			// fn(body.split('=')[1].split('&')[0]);
	    });			
	});

};

exports.isNumber = function(interger) {
  return !isNaN(parseFloat(interger)) && isFinite(interger);
};