var request 	= require('request');
var facebook 	= require('./facebook');
var cheerio 	= require('cheerio');
var utils     	= require('util');
var colors 		= require('colors');
var async 		= require('async');


function getFormData(to, cookie, msg, fn){
		var _that = this;
		
		request({
		    method	: 'GET',
		    uri 	: 'https://m.facebook.com/messages/thread/'+to+'/?refid=17',
	        jar 	: cookie
	    },
	    function(err, res, body) {
	    	// console.log(body);

	        if (err) { return console.log(err) };

	        var $ = cheerio.load(body);
	        $('#composer_form').find('textarea').val( msg )
	        var fields = $('#composer_form').find('textarea, input');

	        var fieldsParsed = {};

	        for( var i = 0; i < fields.length; i++ ){
	        	fieldsParsed[i] = fields[i].attribs;
	        }

	        fn( formatFormSession(fieldsParsed, msg) );
	         
	    });
}
function simpleGet(url, cookie, fn){
	//console.log( timex() + url.bold);
	request({
	    method	: 'GET',
	    uri 	:  url,
        jar 	: cookie
    },
    function(err, res, body) {

        if (err) { return console.log(err) };

       //console.log(res.headers);
        fn( body, res.headers );
       
    });
}

function timex() {

    var tstamp = new Date();
    return (
             tstamp.getHours() + ':' +
             ((tstamp.getMinutes() < 10)
                 ? ("0" + tstamp.getMinutes())
                 : (tstamp.getMinutes())) + ':' +
             ((tstamp.getSeconds() < 10)
                 ? ("0" + tstamp.getSeconds())
                 : (tstamp.getSeconds()))).white.bold.inverse+"\n";
}

function deleteMsg(cookie, tid, infoX, fn){

    request({
	    method: 'POST',
	    uri: 'https://m.facebook.com/messages/action_redirect?tid='+tid+'&start=0&refid=12',
	    form: {
	    	fb_dtsg 		: infoX.fb_dtsg,
	    	charset_test 	: '%E2%82%AC%2C%C2%B4%2C%E2%82%AC%2C%C2%B4%2C%E6%B0%B4%2C%D0%94%2C%D0%84',
	    	"delete"		: "Delete"
	    }, 
        headers:{
        	'user-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
        },
        jar: cookie
    },
    function(err, res, body) {
        if (err) { return console.log(err) };
       	simpleGet(res.headers.location, cookie, function(e){
       		var finalDeleteLink = 'https://m.facebook.com' + e.match(/(\/messages\/action\/\?mm_action=delete&\S{0,73})/g)[0];
       		var fLink = decodeURIComponent(finalDeleteLink).replace(/&amp;/g, '&')

      		simpleGet(fLink, cookie, function(re, headers){
       			fn(re, headers);
       		});
       	});
    });
}

function formatFormSession(items, msg){
	var eos = "";

	eos += "{";
	for( i in items ){

		eos += utils.format( '"%s":"%s",', 
			items[i].name, 
			encodeURIComponent( items[i].value ) 
		); 
	}
	eos += "}";
	eos = JSON.parse(eos.replace(',}', '}'));

	eos.body = msg;
	return eos;
};



function sendMsg(cookie, meta, id, fn){


	var tempMeta = meta;
	console.log('METAAAAAAAAA =>', id);

	request({
	    method	: 'POST',
	    uri 	: 'https://m.facebook.com/messages/send/?icm=1',
	    form 	: meta,
        jar 	: cookie
    },
    function(err, res, body) {
        if (err) { return console.log(err) };


        var locationEnd = res.headers.location;
        if( meta.tids == undefined ) return;

        if( locationEnd.indexOf('send_success') == -1 ){
        	fn(timex() + ' should delete this! => ' + meta.tids.bold.red);
        	

        	deleteMsg(cookie, meta.tids, meta, function(){

				login(userAccount.email, userAccount.password,function(r){
					var cookieFile = r.cookie;

					var DATA = {
						cookie: cookie,
						id: id,
						text: meta.body
					};

		        	composeMessage(DATA, function(){
	        			fn(timex() + ' >>>>>>> Sent! after exluded!!'.bold.red);
	        			return;
	        		});
				});



        	});
        
        }else{
        	fn(timex() + ' >>>>>>> Sent! to => ' + id)
        }
        
		
	});	
}

var getCookie = function(fn){


	// our target to get login cookie
	var loginUrl      = "https://m.facebook.com/login.php";
	// url where cookies will be applied
	var applyCookieTo = "https://facebook.com/";
    request = request.defaults({
        jar: true
    });
	// check if this.coockieJar is already got
	// so use cache instead request another :)
	request({
	    method	: 'POST',
	    uri 	: loginUrl,
	},
	function(err, res, body) {
	    if (err) { return console.log(err) };

	    // header 'set-cookie' property
	    var myCookie = request.cookie(res.headers['set-cookie'][0]);

	    // instace new request jar file
	    var cookieJar = request.jar();
	    // sets our instance with our cookie and set it to our url
	    cookieJar.setCookie(myCookie, applyCookieTo);
	    // cache cookieJar
	    cookieJar = cookieJar;
	    cookieStr = res.headers['set-cookie'][0];

	    // callback...
		fn(cookieJar)
	});		
};

function login(user, pass, fn){

	var myLocation = "";
	var myCookie = "";

	console.log('LKOGIN', user,pass)

		getCookie(function(cookie){
			
		    request({
			    method: 'POST',
			    uri: 'https://m.facebook.com/login.php',
			    form: {
			    	email 	: user,
			    	pass 	: pass,
			    	login	: "Entrar"
			    },
		        headers:{
		        	'user-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
		        }
		    },
		    function(err, res, body) {
		        if (err) { return console.log(err) };
			   	myCookie = request.cookie(res.headers['set-cookie'][0]);
			    var cookieJar = request.jar();

			    cookieJar.setCookie(myCookie, "https://m.facebook.com/");
			    console.log(res.headers.location)
		        fn({
		        	next:res.headers.location, 
		        	cookie: myCookie
		        });
		    });
			//console.log(cookie)
		});

}
function composeMessage(data, fn){

	var cookie = data.cookie;
	var id = data.id;
	getFormData(data.id, cookie, data.text, function(dataF){
		sendMsg(cookie, dataF, id, function(wh){
			fn(wh);
		});
	});
}

/////////////////////// THISSS
var Y = ["100003862593510","100003862593510","100001670353742","100006909975692"];
/*var count = 0;
var end = [];

async.whilst(
    function () { return count < Y.length; },
    function (next) {
		setTimeout(function(){
			login(null, null,function(r){
				var cookieFile = r.cookie;

				var DATA = {
					cookie: cookieFile,
					id: ''+Y[count]+'',
					text: '#{TEXT}'+count
				};

				composeMessage(DATA, function(e){
					end.push(timex() + ''+e); 
				});
			});        

	        count++;
			next();

		},2000)
      
    },
    function (err) {
       console.log(end)
    }
);*/


/*YY("100003862593510", 'TESTE');*/

var userAccount = {};
exports.Send = function (to, msg, account, fn){
	console.log('TO',to)
	userAccount = account[0];
	
	login(account[0].email, account[0].password,function(r){
		var cookieFile = r.cookie;

		var DATA = {
			cookie: cookieFile
		};
		DATA.id = to;
		DATA.text = msg;

		composeMessage(DATA, function(e){
			fn(timex() + ''+e); 
		});
	});
	
}
/*var callbackAlreadyCalled = false;
async.map(Y, function(id, fn){
	setTimeout(function(){
		login(null, null,function(r){
			var cookieFile = r.cookie;

			var DATA = {
				cookie: cookieFile,
				id: id,
				text: '#{TEXT}'
			};

			composeMessage(DATA, function(e){
				if(!callbackAlreadyCalled) {
					callbackAlreadyCalled = true;
					return fn(timex() + ''+e);
				}
				
			});
		});
	},0)

}, function(err, results){
    console.log(results);
});*/
/*
login(null, null,function(r){
	var cookieFile = r.cookie;

	var DATA = {
		cookie: cookieFile,
		id: '100006909975692',
		text: '#{TEXT}'
	};

	composeMessage(DATA, function(e){
		console.log(timex() + ''+e.green); 
	});
})*/