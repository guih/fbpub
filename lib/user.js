var ObjectID   = require('mongoskin').ObjectID;
var validation = require('./validation');
var meta       = require('./group-meta');
var facebook   = require('./facebook');
var mixin      = require('util')._extend;


exports.getUserByEmail = function(handler, email, fn){
	
	var user = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM users WHERE email = ?',email,function(err,rows)
		{

			if(err)
				console.log("Error : %s ",err );

			if( rows.length > 0 ){
				fn(user=rows[0]);
			}else{
				fn({info: "No user with this email.", error: 1});
			}
		
		});

	}); 
};
exports.getUserById = function(handler, id, fn){

    var user = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM users WHERE id = ?',parseInt(id),function(err,rows)
		{

			if(err)
				console.log("Error : %s ",err );

			if( rows.length > 0 ){
				fn(user=rows[0]);
			}else{
				fn({info: "No user with this id.", error: 1});
			}
		
		});

	}); 
};

exports.updateUserById = function(handler, id, postData, fn){
	this.getUserById( handler, id, function(user){
		if( postData.currentPassword == validation.decrypt( user.password )  ){
			if( postData.name != user.name ){
				user.name = postData.name;
			}

			if( postData.address != user.address ){
				user.address = postData.address;
			}
			if( postData.email != user.email ){
				user.email = postData.email;
			}
			if( postData.password != "" ){
				if( postData.password != validation.decrypt( user.password ) ){
					user.password = validation.encrypt( postData.password ); 
				}
			}
			delete user.id;
			console.log(id, user)
			// handler.collection('users').update({_id: ObjectID( user._id )}, user, {w:1}, function(err) {
			// 	if(err){
			// 		throw err;		
			// 	}

			// 	fn({success: 1});
			// });
			handler.getConnection(function (err, connection) {


			connection.query("UPDATE users set ? WHERE id = ? ",[user,id], function(err, rows){

				if (err)
					console.log("Error Updating : %s ",err );

				fn({success: 1});

			});

			});
		}else{
			fn({success: 0});
			return false;
		}

	} );
};
exports.addGroupUser = function(handler, data, fn){
	// console.log('INSERTING THIS DATA=>', data)
    handler.getConnection(function (err, connection) {
        
        
        var query = connection.query("INSERT INTO groups set ? ",data, function(err, rows)
        {

            if (err)
                console.log("Error inserting : %s ",err );

            fn()
        });
            
       // console.log(query.sql); get raw query
    
    });

}
exports.addFbAccount = function(handler, data, fn){
	// console.log('INSERTING THIS DATA=>', data)
    handler.getConnection(function (err, connection) {
        
        
        var query = connection.query("INSERT INTO fb_accounts set ? ",data, function(err, rows)
        {

            if (err)
                console.log("Error inserting : %s ",err );

            fn()
        });
            
       // console.log(query.sql); get raw query
    
    });

}
exports.deleteFbAccount = function(handler, id, fn){
     handler.getConnection(function (err, connection) {
        
        connection.query("DELETE FROM fb_accounts WHERE id = ? ",id, function(err, rows)
        {
            
             if(err)
                 console.log("Error deleting : %s ",err );
            
            fn();
             
        });
        
     });
};



exports.getUserFbAccts = function(handler, id, fn){
    var accts = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM fb_accounts WHERE owner = ?',parseInt(id),function(err,rows)
		{

			if(err)
				console.log("Error : %s ",err );

			if( rows.length > 0 ){
				fn(accts=rows);
			}else{
				fn({info: "No accounts for this user.", error: 1});
			}
		
		});

	}); 

}
exports.getUserFbAcct = function(handler, id, aid, fn){
    var acct = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM fb_accounts WHERE owner = ? and id = ?',[parseInt(id), aid],function(err,row)
		{

			if(err)
				console.log("Error : %s ",err );

			if( Object.keys(row).length > 0 ){
				fn(acct=row);
			}else{
				fn({info: "Error!", error: 1});
			}
		
		});

	}); 

}
exports.sugestFbAccount = function(handler, id, query, fn){
    var accts = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM `fb_accounts` WHERE `owner` = ? AND `name` LIKE ?',[parseInt(id), '%'+query+'%'],function(err,rows)
		{

			if(err)
				console.log("Error : %s ",err );

			if( rows.length > 0 ){
				fn(accts=rows);
			}else{
				fn({info: "No accounts for this user.", error: 1});
			}
		
		});

	}); 

}
exports.getUserGroups = function(handler, id, fn){
    var groups = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM groups WHERE owner = ?',parseInt(id),function(err,rows)
		{

			if(err)
				console.log("Error : %s ",err );

			if( rows.length > 0 ){
				fn(groups=rows);
			}else{
				fn({info: "No group for this user.", error: 1});
			}
		
		});

	}); 

}

exports.getGroupById = function(handler, id, fn){
    var groups = {};
    
	handler.getConnection(function(err,connection){

		connection.query('SELECT * FROM groups WHERE id = ?',parseInt(id),function(err,rows)
		{

			if(err)
				console.log("Error : %s ",err );

			if( rows.length > 0 ){
				fn(groups=rows);
			}else{
				fn({info: "No group wuth this id.", error: 1});
			}
		
		});

	}); 

}
exports.groupExists = function(handler, id, gid, fn){
	this.getUserGroups(handler, id, function(groups){
		// var groupsParsed = JSON.parse(groups);
		// console.log(groups);
		var found = false;
		[].forEach.call(groups, function(val, index){
			if( groups[index].scoped_id == gid){
				found = true;
				return;
			} else {
				found = false;

			}
		})
		if( found == true ){
			fn({ìnfo: 'Group found!', error: 0})
			return;
		} else {
			fn({ìnfo: 'Group not found.', error: 1})

		}
	});

}
exports.groupExistsByName = function(handler, id, groupName, fn){
	// facebook.getAcessToken(function(token) { 
	// 	meta.fetchDetailsById(token, groupName, function(details){
	// 		console.log('GROUPO EXISTS', details)
	// 	});
	// });
	
	this.getUserById( handler, id, function(user){
		// console.log(user)
		var groups = user.groups;
		for(var i=0; i<groups.length; i++){
			if(groups[i].name === groupName){
				fn({state: 1, group: groups[i].id});
				// fn(groups[i]);
				return false;
			}
		}
		fn({state: 0, group: groupName});
		// handler.collection('users').update({_id: ObjectID( user._id )}, user, {w:1}, function(err) {

	});

}


exports.deleteByValue = function(arr ,val) {
    for(var f in arr) {
        if(arr[f].id == val) {
            delete arr[f];
        }
    }
    return arr;
}
exports.deleteGroup = function(handler, id, fn){
     handler.getConnection(function (err, connection) {
        
        connection.query("DELETE FROM groups  WHERE id = ? ",id, function(err, rows)
        {
            
             if(err)
                 console.log("Error deleting : %s ",err );
            
            fn();
             
        });
        
     });
};


exports.updateUserSchedule = function(handler, id, fn){
	
	handler.getConnection(function (err, connection) {
        	// connection.connect();

        var query = connection.query("UPDATE reports set completed = 1 WHERE id = ?", id, function(err, rows)
        {
        	console.log('QURY'. query);

            if (err)
                console.log("Error inserting : %s ",err );

            fn()
        });


 
       // console.log(query.sql); get raw query
    
    });

}
exports.addUserSchedule = function(handler, data, fn){
	console.log(data);
	handler.getConnection(function (err, connection) {
	    var query = connection.query("INSERT INTO reports set ? ",data, function(err, rows)
        {

            if (err)
                console.log("Error inserting : %s ",err );
           
            fn(rows)

 
        });
              
       // console.log(query.sql); get raw query
 
    });

}

exports.fetchSchedules = function(handler, id, fn){
	handler.getConnection(function (err, connection) {

		var sql = [
			'SELECT * FROM ', 
			'reports ' ,
			'INNER JOIN ' ,
			'groups ON reports.group = groups.id ' ,
			'WHERE ' ,
			'reports.owner = ?',
		].join('\n');

	    var query = connection.query(sql, id, function(err, rows)
        {

            if (err)
                console.log("Error inserting : %s ",err );
           
            fn(rows)

 
        });
              
       // console.log(query.sql); get raw query
 
    });
	
}