var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'null0006';

exports.checkEmail = function(handler, email, fn){
	if( !handler ){
		return false;
	}
	var users = {};

    handler.collection('users').find({email: email}).toArray(function (err, items) {
       users = items;
		if( users.length != 0 ){
			fn(true);
		}else{
			fn(false);		
		}
		// console.log(users, users.length)
    });

};

 
exports.encrypt = function(text){
	var cipher = crypto.createCipher(algorithm,password)
	var crypted = cipher.update(text,'utf8','hex')
	crypted += cipher.final('hex');
	return crypted;
};
 
exports.decrypt = function(text){
	var decipher = crypto.createDecipher(algorithm,password)
	var dec = decipher.update(text,'hex','utf8')
	dec += decipher.final('utf8');
	return dec;
};