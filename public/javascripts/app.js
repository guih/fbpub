;(function($){
	var App = function(opts){
		this.init();
		this.hookParsley();
	};

	App.prototype.init = function(){
		this.initListeners();
	}
	App.prototype.initListeners = function(){
		var _that = this;

		var append = ''; 

		var initColors = function(){
			$('input').on('click', function(e){

				$('.description').removeClass('focused');
				$('.item').removeClass('focused');	

				$('.item').removeClass('focused');
				$('.description-input').removeClass('focused');

				$(this).closest('.item').addClass('focused').find('.description-input').addClass('focused');
		

			});
			$('textarea').on('focus', function(e){
				$('.item').removeClass('focused');
				$('.description-input').removeClass('focused');
				
				$('.description').removeClass('focused');
				$('.item').removeClass('focused');			

				$(this).closest('.item').addClass('focused').find('.description').addClass('focused');
			});
		}
		initColors();

		$('.close').on('click', function(){
			$(this).parent().fadeOut();
		});

		$('.register-user').on('click', function(event){
			event.preventDefault();
			
			$('.returned-msg ').hide();

			var registerForm = $('#register-form');
			var data = {};
			var callbackSuccess = function(xhr, status){
				var data = xhr;
				// console.log(data)
				if( data.error ){
					$('.returned-msg ').find('span').text(data.error);
					$('.returned-msg ').fadeIn();
				}
				if( data.status == "ok" ){
					window.location.href = "/register/wait";
				}
				
			};

			registerForm.serializeArray().map(function(i){
				data[i.name] = i.value;
			}); 
			data.level = 0;

			if( $('.parsley-error').length == 0 ){
				$.ajax({
					url: "/register",
					type: "post",
					data: data,
					success: callbackSuccess
				});				
			}
		});

		$('.login-user').on('click', function(event){
			event.preventDefault();	

			$('.returned-msg ').hide();

			var loginForm = $('#login-form');
			var form = loginForm.parsley();
			form.validate();
			var data = {};
			var callbackSuccess = function(xhr, status){
				var data = xhr;
				if( data.error ){
					$('.returned-msg ').find('span').text(data.error);
					$('.returned-msg ').fadeIn();
				}
				if( data.success == 1 ){
					window.location.href = "/new";
				}
				
			};

			loginForm.serializeArray().map(function(i){
				data[i.name] = i.value;
			}); 

			if( $('.parsley-error').length == 0 ){
				$.ajax({
					url: "/login",
					type: "post",
					data: data,
					success: callbackSuccess
				});				
			}	
		});
		$('.edit-user').on('click', function(event){
			event.preventDefault();	

			$('.returned-msg ').hide();

			var editForm = $('#edit-user');

			var form = editForm.parsley();
			form.validate();
			var data = {};
			var callbackSuccess = function(xhr, status){
				var data = xhr;
				console.log(xhr)
				if( data.success == 1 ){
					window.location.href = "/me?updated";
				}
				if( data.success == 0 ){
					$('.returned-msg').find('span').text(data.msg);
					$('.returned-msg').fadeIn();					
				}
				
			};

			editForm.serializeArray().map(function(i){
				data[i.name] = i.value;
			}); 

			if( $('.parsley-error').length == 0 ){
				$.ajax({
					url: "/me",
					type: "post",
					data: data,
					success: callbackSuccess
				});				
			}	
		});
		$('.set-group').on('click', function(event){
			event.preventDefault();	

			$('.returned-msg ').hide();
			$('.group-load').show();


			var groupForm = $('#set-group');

			var form = groupForm.parsley();
			form.validate();
			var data = {};
			var callbackSuccess = function(xhr, status){
				$('.group-load').hide();

				var data = xhr;
				if( data.info ){
					window.location.href = "/new/" + data.alias;					
				}
				if( data.error ){
					$('.returned-msg').find('span').text(data.error);
					$('.returned-msg').fadeIn();					
				}
				
			};

			groupForm.serializeArray().map(function(i){
				// data[i.name] = i.value;
				data = encodeURIComponent( i.value);
			}); 
			console.log(data)

			if( $('.parsley-error').length == 0 ){
				$.ajax({
					url: "/group/"+data,
					type: "get",
					success: callbackSuccess
				});				
			}	
		});
        $('.tp').tooltipster({
            contentAsHTML: true
        });


		var lock = true;
		$('.start-messaging').on('click', function(event){
			event.preventDefault();	

			$('.returned-msg ').hide();
			

			var messageForm = $('#message-form');
			var form = messageForm.parsley();
			form.validate();
			var data = {};
			var callbackSuccess = function(xhr, status){
				var data = xhr;
				console.log(xhr)
				lock = true;
				$('.load-send ').hide();
			 	$('.returned-msg ').find('span').text(data.info);
			 	$('.returned-msg ').fadeIn();
				// if( data.error ){
				// 	$('.returned-msg ').find('span').text(data.error);
				// 	$('.returned-msg ').fadeIn();
				// }
				// if( data.success == 1 ){
				// 	window.location.href = "/new";
				// }
				
			};

			if( lock == false ) return;

			data = serializeEspecialForm();

			// messages[Math.floor(Math.random()*messages.length)];

			console.log(data)
			if( $('.parsley-error').length == 0 && lock == true ){
				lock = false;
				$('.load-send ').show();
				$.ajax({
					url: "/new",
					type: "post",
					data: data,
					success: callbackSuccess
				});				
			}	
		});

		var serializeEspecialForm = function(){
			var form = $('#message-form').serialize();

			var items = decodeURIComponent(form).split('&');
			var serialized = {};
			var itemsY = [];
			
			for(var i = 0; i<items.length; i++)
				itemsY.push(items[i].split('='));

			for(var j = 0; j < itemsY.length; j++)
				serialized[itemsY[j][0]] = itemsY[j][1];

			if( Object.keys(serialized).length > 0 )
				return serialized;
			
		}

		$('.push-fb-acct').on('click', function(event){
			event.preventDefault();	

			$('.returned-msg ').hide();
			var fbAcct = $('#add-acct-form');
			var form = fbAcct.parsley();
			form.validate();
			var data = {};
			var callbackSuccess = function(xhr, status){
				$('.add-fb-load').fadeOut();
				var data = xhr;
				if( data.error ){
					$('.returned-msg ').find('span').text(data.error);
					$('.returned-msg ').fadeIn();
				}
				if( data.success == 1 ){
					$('.returned-msg ').addClass('success');
					$('.returned-msg ').find('.close').addClass('right');
					
					$('.returned-msg ')
					.find('.close')
					.find('i')
					.removeClass('fa-times')
					.addClass('fa-check');

					$('.returned-msg ').find('span').text('Conta adicionada!');
					$('.returned-msg ').fadeIn();

					setTimeout(function(){
						window.location.href = "/fb";
					},2000);
				}
				
			};

			fbAcct.serializeArray().map(function(i){
				data[i.name] = i.value;
			}); 

			if( $('.parsley-error').length == 0 ){
				$('.add-fb-load').fadeIn();
				$.ajax({
					url: "/fb/add",
					type: "post",
					data: data,
					success: callbackSuccess
				});				
			}	
		});

		$('.add-msg-field').on('click', function(){
			if( $('.msg-container').length > 3 ){
				return;
			}
			var tlp = "";
			var tlp = $('.msg-container:not(.clone-x)').clone();
			var currentId = parseInt(($('.msg-container').length+1));			
			var textArea = tlp.find('textarea')[0];

			textArea.setAttribute('name', "msg["+currentId+"]");
			tlp.addClass('clone-x');

			$('.msg-container:not(.clone-x)').after(tlp);
			initColors();
		});


		$('#fb-acct-get').on('blur', function(){
			if( $('.sugest').is(':visible') ){
				$('.sugest').fadeOut();
			}
		});
		$('#fb-acct-get').on('keyup', function(){
			var field = $('.sugest').parent();
			if(field.find('.fa').hasClass('fa-check')){
				$('#fb-acct-get').css('font-weight', 'normal');

				field.find('.fa').addClass('fa-search').removeClass('fa-check');
			}
			var query = $(this).val();
			var last_querie = "";

			if( query == "" ){
				if( $('.sugest').is(':visible') ){
					$('.sugest').fadeOut();
				}
			}
			$.getJSON('/new/fb/'+query, function(res){
				if(res){
					if( ! res.error ){
					
						var markup = "";

						for( var i = 0; i < res.length; i++ ){	
							console.log(res[i])
							markup += '<li>'
							markup += '	<a href="#" data-account="'+res[i].id+'"> ';
							markup += '		<img src="'+res[i].image+'" alt="'+res[i].name+'">';
							markup += '		<span>'+res[i].name+'</span>';
							markup += '	</a>';
							markup += '</li>';
						}
						$('.sugest').fadeIn();
					
						$('.sugest').empty().append(markup);
					}else{
						$('.sugest').fadeIn();

						$('.sugest').empty().append('<p>Nada econtrado com termo "'+query+'"</p>');
					}
				}
	
				$('.sugest').find('li').on('click', function(e){
					$('.acct').val( $(this).find('a').data('account') );
					$('#fb-acct-get').val( $(this).find('span').text() );
					$('#fb-acct-get').css('font-weight', 'bold');
					$('.sugest').fadeOut(function(){
						$('.sugest').parent().find('.fa').removeClass('fa-search').addClass('fa-check');
					});

				});
			});

			//$('.sugest').empty().append('R')
		});
		$('.time').datetimepicker({
			startDate:'+2015/05/01'
		});
	};
	App.prototype.hookParsley = function(){
		window.ParsleyConfig = {
			classHandler: function ( elem, isRadioOrCheckbox ) {
				return $(elem).parents(".form-group");
			},
			errorsWrapper: '',
			errorTemplate: '' 
		};
		this.hookUp();

	}
	App.prototype.hookUp = function(){
		$.fn.serializeAssoc = function() {
		  var data = {};
		  $.each( this.serializeArray(), function( key, obj ) {
		    var a = obj.name.match(/(.*?)\[(.*?)\]/);
		    if(a !== null)
		    {
		      var subName = a[1];
		      var subKey = a[2];
		      if( !data[subName] ) data[subName] = [ ];
		        if( data[subName][subKey] ) {
		          if( $.isArray( data[subName][subKey] ) ) {
		            data[subName][subKey].push( obj.value );
		          } else {
		            data[subName][subKey] = [ ];
		            data[subName][subKey].push( obj.value );
		          }
		        } else {
		          data[subName][subKey] = obj.value;
		        }
		      } else {
		        if( data[obj.name] ) {
		          if( $.isArray( data[obj.name] ) ) {
		            data[obj.name].push( obj.value );
		          } else {
		            data[obj.name] = [ ];
		            data[obj.name].push( obj.value );
		          }
		        } else {
		          data[obj.name] = obj.value;
		        }
		      }
		    });
		    return data;
		};
	}

	$(function(){
		return new App();
	})
})(jQuery);