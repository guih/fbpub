
var express  = require('express');
var router   = express.Router();
var userCtrl = require('../lib/user');
var facebook = require('../lib/facebook');
var inbox = require('../lib/msg-fb');
var colors = require('colors');

var schedule = require('node-schedule');
var async    = require('async');



/* GET new ad page. */
router.get('/', function(req, res) {
	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}
	console.log( req.session.userId)
	// facebook.getAcessToken();
	var db = req.db;
	userCtrl.getUserById( req, req.session.userId, function(data){
		res.render('new-ad', { 
			title: 'Novo anúncio', 
			'page' : 'new_advertising',
			user_id: req.session.userId,
			user_data: data,
			user_logged: true,
			user_meta: JSON.stringify( data )
		});
	});
});
router.get('/fb/:query',  function(req, res){
	var query = req.params.query;
	
	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}

	console.log('QUERY =>', query)
	if( query == "" ){
		res.redirect('/new')
	}

	var id = req.session.userId;


	userCtrl.sugestFbAccount( req, id, query, function(accounts){
		res.json(accounts);
	} )
});


router.get('/:gid', function(req, res){
	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}


	var groupId = req.params.gid || "";
	var id = req.session.userId;
	var db = req.db;

	if( groupId == "fb/" || groupId == "fb" ){
		res.redirect('/new');
		return;
	}
	userCtrl.getUserById( req, id, function(data){
		userCtrl.getUserGroups( req, id, function(meta){

			var myGroup = {};
		// 	var groups = meta.groups;
		// 	var desiredGroup = {};
		// // 	console.log(groups)

			// for(var i=0; i<groups.length; i++){
			// 	if(groups[i].id === groupId){
			// 		desiredGroup = groups[i]; 
			// 		break;
			// 	}
			// }
			// console.log('DEESIRED GROUP =>',desiredGroup)
			desiredGroup = meta;
			// console.log(meta,'<=META');
			[].forEach.call(desiredGroup, function(val, index){
				desiredGroup[index].ids = JSON.parse(meta[index].ids)
				if( groupId == desiredGroup[index].scoped_id ){
					myGroup = desiredGroup[index];
				}
				console.log(groupId == desiredGroup[index].scoped_id)
			});


			
			if( Object.keys(desiredGroup).length !== 0 || Object.keys(myGroup) !== 0){
				res.render('new-ad-internal', { 
					title: 'Novo anúncio para ' + myGroup.name,
					'page' : 'new_advertising',
					user_id: req.session.userId,
					user_data: data,
					user_logged: true,
					// user_data: meta,
					group: myGroup,
					groupS: JSON.stringify(myGroup)
				});
			}else{
				res.redirect('/new?error')
			}
			console.log(groupId, myGroup)
		} ) 
	});

});
router.post('/', function(req, res){

	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}
	var groupId = req.body.id;
	var id = req.session.userId;
	var messages = req.body.msg;

	var randomMsg = messages[ Math.floor(Math.random() * messages.length) + 0 ];

	//console.log(req.body);
	//res.json({sent: 0})
	//console.log(req.body)

	userCtrl.getUserById( req, id, function(data){
		userCtrl.getUserGroups( req, id, function(meta){
			if( req.body.timex != "" ){
				var formTimex = req.body.timex.split(" ");
				var endTimex = formTimex[0].split('/');
				var finalTimex = [endTimex[1], endTimex[2], endTimex[0]].join(" ");
				userCtrl.addUserSchedule(req, {
					owner: req.session.userId,
					completed: 0,
					messages: JSON.stringify( messages ),
					group: parseInt( groupId ),
					when: [finalTimex, formTimex[1]].join(" "),
					iie: false
				}, function(){
					//console.log(myGroup)
					var job = new schedule.Job(function() {
						console.log('Run after %s minute(s).', [finalTimex, formTimex[1]].join(" "));
					});
					job.schedule([finalTimex, formTimex[1]].join(" "));
				});

				res.json({info:"Adicionado à fila.", sucess: true});
			
				// console.log( [i, formTimex[1]].join(" "), new Date([i, formTimex[1]].join(" ")) )
			}
			var myGroup = {};
			var myIds = [];

		// 	var groups = meta.groups;
		// 	var desiredGroup = {};
		// // 	console.log(groups)

			desiredGroup = meta;

			// console.log('DEESIRED GROUP =>',desiredGroup)
			
			// console.log(meta,'<=META');
			
			[].forEach.call(desiredGroup, function(val, index){
				desiredGroup[index].ids = JSON.parse(meta[index].ids)
				if( groupId == desiredGroup[index].id ){
					myGroup = desiredGroup[index];
				}
			});

			 console.log('OLAOLAE', myGroup, groupId);
			
			for( var j = 0; j < myGroup.ids.length; j++ )
				myIds.push(myGroup.ids[j]);


			/*async.map(myIds,function(y,callback){
				var callbackAlreadyCalled = false;
				inbox.Send( y,decodeURIComponent( randomMsg.replace(/\+/g, '%20')), function(data){
					if(!callbackAlreadyCalled) {
						callbackAlreadyCalled = true;
						callback(null,data); 
					}
				});
			},function(error,results){
			    if(error){ console.log('Error!'); return; }
			    console.log(results);
			    // do stuff with results
			});*/
function updateResult(id){

	userCtrl.updateUserSchedule(req, id, function(){
	})


}

 /*numCompletedCalls = 0
 for(var j=0; j<elements.length; j++)
 {
    callDatabase(function(results) {
       numCompletedCalls++;
       if (numCompletedCalls == elements.length)
          console.log("Done all calls!");

    });
 }*/


 		var sCount = 0;
		if( req.body.timex ==  "" ){
 			userCtrl.getUserFbAcct(req, id, req.body.account, function(account){
				userCtrl.addUserSchedule(req, {
					owner: req.session.userId,
					completed: 0,
					messages: JSON.stringify( messages ),
					group: parseInt( groupId ),
					when: 'null',
					iie: true


				}, function(row){
					/*function Assync(i) {
						var item = myIds[i];

					  if( i < myIds.length ) {
						inbox.Send( item, decodeURIComponent( randomMsg.replace(/\+/g, '%20')), account, function(data){
							Assync( i + 1 );
						});
					  }else{
					  	console.log('done!'.green)
					  }
					}
					Assync(0);*/

					for( var o =0; o<=myIds.length; o++ ){
						console.log('TOOOOOOOOOOOOOOOOOOOOOOOOO', myIds[o]);
			
							inbox.Send( myIds[o],decodeURIComponent( randomMsg.replace(/\+/g, '%20')), account, function(data){
								console.log(data);
							});
					       sCount++;
					       	if (sCount == (myIds.length-1))
					        	console.log("Done all calls!");
							

							console.log( 'IDSSSSSSSSSSSSSSs$$$$$$$$$$$$$$$$$$$$$$$'.red,o,(myIds.length-1) )
		
						
					}
/*					async.map(myIds,function(y,callback){
						var callbackAlreadyCalled = false;

					},function(error,results){
					    if(error){ console.log('Error!'); return; }
					    console.log(results);
						updateResult(row.insertId)
					})	*/

					res.json({
						info:"As mensagens serão entregues em breve!", 
						sucess: true
					});
				})
			})


			/*	var callbackAlreadyCalled = false;

				async.waterfall([
				    function(callback) {
				    	var msg  = decodeURIComponent( randomMsg.replace(/\+/g, '%20'));
						userCtrl.addUserSchedule(req, {
							owner: req.session.userId,
							completed: 0,
							messages: JSON.stringify( messages ),
							group: parseInt( groupId ),
							when: 'null',
							iie: true


						}, function(row){
							callback(null, row.insertId, msg, myIds);
						});
				        
				    },
				    function(id, msg, myIds, callback) {
				    	for( var i = 0; i < myIds.length; i++ ){

							inbox.Send( myIds[i],msg, function(data){
								if(!callbackAlreadyCalled) {
									callbackAlreadyCalled = true;
									callback(null,id, data); 
									
								}
							});
				    	}
				    },
				    function(id,data, callback) {
				        // arg1 now equals 'three'
				        callback(null, id, data);
				    }
				], function (err, result) {
					updateResult(id)
					res.json({
						info:"As mensagens serão entregues em breve!", 
						sucess: true
					});   
				});
*/
			
		}


			// }else{
			// 	res.json({error:'/new?error'})
			// }

		} ) 
	});


	// userCtrl.getUserById( req.db, id, function(meta){
	// 	var groups       = meta.groups;
	// 	var desiredGroup = {};

	// 	// for(var i=0; i<groups.length; i++){
	// 	// 	if(groups[i].id === groupId){
	// 	// 		desiredGroup = groups[i]; 
	// 	// 		break;
	// 	// 	}
	// 	// }
	// 	// facebook.sendMessage( desiredGroup.data, req.body.text_message, function(re){
	// 	// 	console.log('REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE => ', re)
	// 	// } )
	// 	console.log(desiredGroup.data, req.body)
	// });
});
module.exports = router;
