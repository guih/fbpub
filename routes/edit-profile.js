
var express    = require('express');
var router     = express.Router();
var userCtrl   = require('../lib/user');
var validation = require('../lib/validation');
var ObjectID   = require('mongoskin').ObjectID;



/* GET edit-profile page. */
router.get('/', function(req, res) {
  	if( ! req.session.userId ){
		res.redirect('/');
	}
	userCtrl.getUserById( req, req.session.userId, function(data){
		res.render('edit-profile', { 
		  	title: 'Meus dados', 
		  	'page' : 'edit_profile',
			user_id: req.session.userId ,
			user_data: data,
			user_logged: true
		});
	});
});


/* POST edit-profile page. */
router.post('/', function(req, res) {
  	if( ! req.session.userId ){
		res.redirect('/');
	}

	var db      = req.db;
	var id      = req.body.uid;
	var newData = {};
	var data    = req.body;


	userCtrl.updateUserById( req, id, data, function(result){
		console.log(result)
		if( result.success == 1 ){
			res.send({success: 1})
		}else{
			res.send({success: 0, msg: "Senha antiga não reconhecida. Tente novamente."})
		}
	} );

});
module.exports = router;
