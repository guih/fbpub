var express = require('express');
var router  = express.Router();
var fb      = require('../lib/facebook');




/* GET to send message
*   id (int) User profile ID
*   message (string) text should be send
*/
router.get('/:id', function(req, res) {
	// User profile ID
	var id = req.params.id || "";
	// text should be send
	var message = req.query.message || "";

	// Simple validations, check if its empty
	if( id == "" ){
		res.json({error: "Invalid params."});
	}
	if( message == "" ){
		res.json({error: "Too short message."});
	}

	// Call senMessage method form our facebook access lib
	// return a callback with status
	fb.sendMessage(id, message, function(data){
		res.send(data);
	});

});

module.exports = router;
