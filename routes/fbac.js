var express    = require('express');
var router     = express.Router();
var validation = require('../lib/validation');
var user   = require('../lib/user');
var request = require('request');
var facebook   = require('../lib/facebook');




/* GET home page. */
router.get('/', function(req, res) {

	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}


	user.getUserById( req, req.session.userId, function(data){
		user.getUserFbAccts(req, data.id, function(accounts){
			res.render('fb', { 
				title: 'Contas do facebook', 
				page : 'fb' ,
				user_id: req.session.userId,
				user_data: data,
				user_logged: true,
				fb_accounts: accounts
			});
		});
	});
});
/* GET delete page. */
router.get('/delete/:id', function(req, res) {

	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}

	var params = req.params;


	user.deleteFbAccount(req, params.id, function(){
		res.redirect('/fb');
	});
});
router.post('/add', function(req, res) {
	var db            = req.db;
	

	var getCookie = function(fn){

		console.log('get cookie')
		// our target to get login cookie
		var loginUrl      = "https://m.facebook.com/login.php";
		// url where cookies will be applied
		var applyCookieTo = "https://facebook.com/";
	    request = request.defaults({
	        jar: true
	    });
		// check if this.coockieJar is already got
		// so use cache instead request another :)
		request({
		    method	: 'POST',
		    uri 	: loginUrl,
		},
		function(err, res, body) {
		    if (err) { return console.log(err) };

		    // header 'set-cookie' property
		    var myCookie = request.cookie(res.headers['set-cookie'][0]);

		    // instace new request jar file
		    var cookieJar = request.jar();
		    // sets our instance with our cookie and set it to our url
		    cookieJar.setCookie(myCookie, applyCookieTo);
		    // cache cookieJar
		    cookieJar = cookieJar;
		    cookieStr = res.headers['set-cookie'][0];

		    // callback...
			fn(cookieJar)
		});		
	};

	function login(user, pass, fn){
		console.log('login')
		var myLocation = "";
		var myCookie = "";


			getCookie(function(cookie){
				
			    request({
				    method: 'POST',
				    uri: 'https://m.facebook.com/login.php',
				    form: {
				    	email 	: user,
				    	pass 	: pass,
				    	login	: "Entrar"
				    },
			        headers:{
			        	'user-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
			        },
			        jar: cookie
			    },
			    function(err, res, body) {
			        if (err) { return console.log(err) };
				   	myCookie = request.cookie(res.headers['set-cookie'][0]);
				    var cookieJar = request.jar();
				    var bkpRes = res;

				    cookieJar.setCookie(myCookie, "https://m.facebook.com/");

				    request({
				    	method: "GET",
						headers:{
							'user-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
						},
				    	uri: "https://m.facebook.com/profile.php",
				    	jar: cookie
				    }, function(err, res, body){
 						if (err) { return console.log(err) };

 						var yEnd = body.match(/(,"all":\[{"id":(.+?),"url":"\\\/profile)/g);
 						var dEnd = "";

 						console.log(bkpRes.headers.location)
 						if( yEnd != null ){
 							dEnd = JSON.parse(yEnd[0]
								.split('all":')[1]
								.split(',"url":"')[0] + "}]"
							)[0];
 						}
						fn({
							next:bkpRes.headers.location, 
							uinfo: dEnd
						});
				    })
			    });


				//console.log(cookie)
			});

	}
	console.log(req.body)
	login( req.body.email, req.body.passwd, function(r){
		console.log('RRRRRRRRRRRR', r);
		if(r.next.indexOf('https://m.facebook.com/login.php?') !== -1){
			res.json({
				error: "Login inválido. Tente novamente."
			});
			return;
		}else{
			var postdata = req.body;
			var toInsert = {
				email: postdata.email,
				password: postdata.passwd,
				added_on: postdata.added_on,
				owner: postdata.owner,
				name: r.uinfo.name,
				image: r.uinfo.pic,
				uid: r.uinfo.id
			};
		user.getUserFbAccts(req, postdata.owner, function(accounts){
			for( var j = 0; j < accounts.length; j++ ){
				if( accounts[j].email == postdata.email && accounts[j].owner == postdata.owner ){
					res.json({
						error: "Este login já foi cadastrado."
					});
					return;					
				}
			}
			user.addFbAccount(req, toInsert, function(){
				res.json({
					success: 1
				});			
			});
		});

		}

		console.log(r);
	} )
/*	userCtrl.getUserByEmail(req, email, function(user){
		// console.log(user).
		if( user.error ){
			res.send({error: "Email não reconhecido."});
			return false;
		}else{
			// console.log(user.password)
			var decryptedPassword = validation.decrypt( user.password );
			console.log(decryptedPassword, plainPassword, user.password);
			if( decryptedPassword == plainPassword ){
				req.session.userId    = user.id;
				req.session.userToken = user.id;
				// console.log('OK!');
				res.send({success: 1});

			}else{
				// console.log('NOT OK!');

				res.send({error: "Senha incorreta."});				
				return false;				
			}		
		}
	});*/
});


module.exports = router;
