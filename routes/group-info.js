var express   = require('express');
var router    = express.Router();
var groupInfo = require('../lib/group-meta');
var user      = require('../lib/user');
var getMebers = require('../lib/get-members');


/* GET group id and its members
*   gname (string) str name of group
*/
router.get('/:gname', function(req, res, next) {
	if( ! req.session.userId ){
		res.redirect('/');
	}
	var myRes = res;
// res.setTimeout(120000, function(){
	// Group slug
	var groupLink = req.params.gname || "";
	var groupSlug = groupLink.split('/')[4];

	// console.log(groupLink, groupLink.split('/'), groupLink.split('/')[4])	
	if(  groupInfo.isNumber( groupSlug ) ){
		extractIds( groupSlug )
	}else{
		groupInfo.getGroupByName(groupSlug, function(gId){
			extractIds( gId )
		});
		console.log('get numeric id...');
	}
	// Simple validation
	if( groupLink == "" ) {
		res.json({error: "Invalid params."});
	}
	function extractIds(gId){
		user.groupExists(req, req.session.userId, gId, function(e){
			if(e.error == 0){
				res.json({info: 'Group exists', alias: gId});
			}else{
				console.log('fetch group!');

				getMebers.getMembers(req.db, gId, function(resp){
					console.log(resp);
					var groupInsertion = {
						'scoped_id'	:resp.groupId,
						'name'		:resp.title,
						'total'		:resp.memberCount,
						'fetched'	: resp.ids.length,
						'added_on'	: ( +new Date() ),
						'owner'		:req.session.userId,
						'ids'		:JSON.stringify(resp.ids),		
					}
					// console.log('RESP=>', resp,groupInsertion)
					user.addGroupUser(req, groupInsertion, function(){
						myRes.json({info: "fetched", alias:groupInsertion.scoped_id})
					})
				} );
			}
		});
	};
		
    // });

    // next();

	// if( groupInfo.isNumber( groupSlug ) ){
		// user.groupExists( req.db, req.session.userId, groupSlug, function(resp){
		// 	if( resp.state == 0){
		// 		groupInfo.seek( groupSlug, null, function(data){
		// 			console.log(data);
		// 			res.json(data);
					
		// 			if( !data.error ){
		// 				user.addGroupUser(req.db, data, req.session.userId, function(){});
		// 			}
					
		// 		} );			
		// 	}else{
		// 		res.json({info: "group found!", alias: groupSlug});
		// 		// console.log(resp)
		// 	}

		// 	// 	console.log('DATA',groups, meta.groups)

		// 	// 	db.collection('users').update({email: meta.email}, { $set: { groups: groups } }, function(err, re){
		// 	// 		res.redirect('/groups');
		// 	// 	})
		// 	// 	// user.addGroupUser(req.db, data, id, function(){});
		// } );
			// return;
		// Call method seek form our group-meta lib



	
  	// res.send('respond with a resource');
});

module.exports = router;
