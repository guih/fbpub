var express = require('express');
var router  = express.Router();
var user      = require('../lib/user');
var ObjectID   = require('mongoskin').ObjectID;


/* GET groups page. */
router.get('/', function(req, res) {
	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}

	// 	res.render('new-ad', { 
	// 		title: 'Novo anúncio', 
	// 		'page' : 'new_advertising',
	// 		user_id: req.session.userId,
	// 		user_data: data,
	// 		user_logged: true,
	// 		user_meta: JSON.stringify( data )
	// 	});
	// });
	user.getUserGroups(req, req.session.userId, function(groups){
		// console.log(groups)
		[].forEach.call(groups, function(val, index){
			groups[index].ids = JSON.parse(groups[index].ids);
		});
		user.getUserById( req, req.session.userId, function(data){
			res.render('groups', { 
				title: 'Grupos cadastrados', 
				page : 'groups' ,
				user_id: req.session.userId,
				groups: groups,
				user_data: data,
				user_logged: true,

			});
		});
	});
});
router.get('/delete/:gid', function(req, res){
	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}else{
		var groupId = req.params.gid || "";
		var id = req.session.userId;
		var db = req.db;
		user.deleteGroup(req, groupId, function(){
			res.redirect('/groups');
		})
		// user.getUserById( req.db, id, function(meta){
		// 	var data = meta.groups;
		// 	var groups = meta.groups;
		// 	console.log(groups)

		// 	for(var i=0; i<groups.length; i++){
		// 		if(groups[i].id === groupId){
		// 			groups.splice(i, 1);  
		// 			break;
		// 		}
		// 	}
		// 	console.log('DATA',groups, meta.groups)

		// 	db.collection('users').update({email: meta.email}, { $set: { groups: groups } }, function(err, re){
		// 		res.redirect('/groups');
		// 	})
		// 	// user.addGroupUser(req.db, data, id, function(){});
		// } )		
	}

});

module.exports = router;
