var express    = require('express');
var router     = express.Router();
var validation = require('../lib/validation');
var userCtrl   = require('../lib/user');
var facebook   = require('../lib/facebook');




/* GET home page. */
router.get('/', function(req, res) {
	// req.db.on('error', function(){
	//     res.redirect('/')
	// });
    // req.getConnection(function (err, connection) {
        
        
    //     var query = connection.query("INSERT INTO t_user set ? ",{a:'sssss'}, function(err, rows)
    //     {
  
    //       if (err)
    //           console.log("Error inserting : %s ",err );

          
    //     });
        
    //    // console.log(query.sql); get raw query
    
    // });
	var indexPage = { title: 'Express', 'page':'', user_logged: false };

	if( req.session.userId ){
		res.redirect('/new')
	} 

  	res.render('index', indexPage);


});

/* GET home page. */
router.post('/login', function(req, res) {
	var db            = req.db;
	
	var email         = req.body.email;
	var plainPassword = req.body.password;
	var user          = {};

	
	userCtrl.getUserByEmail(req, email, function(user){
		// console.log(user).
		if( user.error ){
			res.send({error: "Email não reconhecido."});
			return false;
		}else{
			// console.log(user.password)
			var decryptedPassword = validation.decrypt( user.password );
			console.log(decryptedPassword, plainPassword, user.password);
			if( decryptedPassword == plainPassword ){
				req.session.userId    = user.id;
				req.session.userToken = user.id;
				// console.log('OK!');
				res.send({success: 1});

			}else{
				// console.log('NOT OK!');

				res.send({error: "Senha incorreta."});				
				return false;				
			}		
		}
	});
});

module.exports = router;
