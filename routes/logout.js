
var express = require('express');
var router = express.Router();

/* GET new ad page. */
router.get('/', function(req, res) {
	if( ! req.session.userId ){
		res.redirect('/');
	}else{
		req.session.destroy(function(err) {
			res.redirect('/')
		});
	}
});

module.exports = router;
