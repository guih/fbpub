var express    = require('express');
var router     = express.Router();
var validation = require('../lib/validation')

 

// Error cheatsheet
// 1 password mismatch
// 2 email taken
// 3 empty field


/* GET register page. */
router.get('/', function(req, res) {
  res.render('register', { 
  	title: 'Cadastro de usário', 
  	'page' : 'register_user',    
    user_id: req.session.userId 
  });
});

router.get('/wait', function(req, res) {
    res.render('wait', {});
});
// /*
//  * GET userlist.
//  */
// router.get('/userlist', function(req, res) {
//     var db = req.db;
//     db.collection('users').find().toArray(function (err, items) {
//         res.json(items);
//     });
// });

/*
 * POST to adduser.
 */
router.post('/', function(req, res) {
    var db = req.db;

    var isValid; 
    var email = req.body.email;
    var data  = req.body;

    console.log(req.body.comfirmPassword == req.body.password, req.body.comfirmPassword, req.body.password)
    
    if( req.body.password.length < 3 ){
        res.send({
            error: "Senha curta demais",
            level: 3,
        });     
        return;
    } 


    if( req.body.name.length < 3 ){
        res.send({
            error: "Nome inválido",
            level: 3,
        });     
        return;
    } 
    if( ! /(.+)@(.+){2,}\.(.+){2,}/.test( req.body.email ) ){
        res.send({
            error: "email inválido",
            level: 3,
        });     
        return;
    } 
    if( req.body.comfirmEmail != req.body.email ){
        res.send({
            error: "Email de comfirmação precisa ser identico ao email desejado.",
            level: 3,
        });     
        return;       
    }
    if( req.body.address.length < 3 ||req.body.address.length == ""){
        res.send({
            error: "Endereço inválido",
            level: 3,
        });     
        return;
    } 
    if( req.body.comfirmPassword !== req.body.password ){
        res.send({
            error: "Senhas não conferem",
            level: 1,
        });     
        return;
    } 

    data.password = validation.encrypt( data.password );
    console.log(data.password, validation.decrypt(data.password));
    delete data.comfirmPassword;
    delete data.comfirmEmail;
    // data.groups = [];
    console.log(data);
        req.getConnection(function (err, connection) {
        
        
    var query = connection.query("INSERT INTO users set ? ",data, function(err, rows)
    {

        if (err)
            console.log("Error inserting : %s ",err );
        
        if(err === null) {
            end = { msg: 'Usuário cadastrado com sucesso!', status: 'ok', error: 0 }

        } else{

            end = { msg: 'Tente novamente', status: 'fail', error: 1 }
        }
        res.send(end)

    });
        
       // console.log(query.sql); get raw query
    
    });
    /****************/

    // validation.checkEmail( db, email, function(invalid){
    //     if(invalid === true){
    //         res.send({
    //             error: "Email em uso",
    //             level: 2,
    //         });
    //     }else{
    //         db.collection('users').insert(data, function(err, result){
    //             var end = {};
    //             if(err === null) {
    //                 end = { msg: 'Usuário cadastrado com sucesso!', status: 'ok', error: 0 }
  
    //             } else{

    //                 end = { msg: 'Tente novamente', status: 'fail', error: 1 }
    //             }
    //             res.send(end)
                
    //         });
    //     }
    // });
    /***************/

});

// /*
//  * DELETE to deleteuser.
//  */
// router.delete('/deleteuser/:id', function(req, res) {
//     var db = req.db;
//     var userToDelete = req.params.id;
//     db.collection('users').removeById(userToDelete, function(err, result) {
//         res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
//     });
// });


module.exports = router;