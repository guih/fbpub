var express = require('express');
var userCtrl = require('../lib/user');

var router = express.Router();

/* GET sent page. */
router.get('/', function(req, res) {
	if( ! req.session.userId ){
		res.redirect('/');
		return;
	}

	var userId = req.session.userId;
	var groupsScheduled = [];

	userCtrl.getUserById(req, userId, function(user){
		userCtrl.fetchSchedules( req, userId, function(schedules){


			res.render('sent', { 
				title: 'Relatório de envios', 
				'page' : 'sent_messages',
				user_id: req.session.userId,
				user_logged: true,
				user_data: user,
				schedules: schedules
			});
		} );
	})

});

module.exports = router;


